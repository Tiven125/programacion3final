
package com.arbolesprog3.modelo.abb;

import java.io.Serializable;


public class NodoABB implements Serializable {
    private Celular dato;
    private NodoABB izquierda;
    private NodoABB derecha;

    public NodoABB(Celular dato) {
        this.dato = dato;
    }

    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public NodoABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoABB izquierda) {
        this.izquierda = izquierda;
    }

    public NodoABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoABB derecha) {
        this.derecha = derecha;
    }
    
    
    public boolean esHoja()
    {
        return izquierda==null && derecha==null;
    }
    
    
    
    
    
}
