
package com.arbolesprog3.modelo.abb;

import java.io.Serializable;

public class Marca implements Serializable{
    private short codigo;
    private String descripcion;

    public Marca() {
    }

    
    
    public Marca(short codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public short getCodigo() {
        return codigo;
    }

    public void setCodigo(short codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Marca{" + "codigo=" + codigo + ", descripcion=" + descripcion + '}';
    }

    
    }
    
    
    

