/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.Batalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiven
 */
public class ArbolBatallaBarcoNario implements Serializable {

    NodoBatallaNario raiz;
    int cantidadNodos;
    List<Coordenada> coordendas;

    public ArbolBatallaBarcoNario() {
        coordendas = new ArrayList<>();
    }

    public NodoBatallaNario getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBatallaNario raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public List<Coordenada> getCoordendas() {
        return coordendas;
    }

    public void setCoordendas(List<Coordenada> coordendas) {
        this.coordendas = coordendas;
    }

    public boolean esHoja(NodoBatallaNario nodo) {
        if (nodo.equals(raiz)) {
            return false;

        }

        return nodo.getHijos() == null;

    }

    public NodoBatallaNario buscarNodoxNombre(String nombre) {
        if (raiz != null) {
            return buscarNodoxNombre(nombre, raiz);
        }
        return null;
    }

    private NodoBatallaNario buscarNodoxNombre(String nombre, NodoBatallaNario pivote) {
        if (pivote.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return pivote;
        } else {
            for (NodoBatallaNario hijo : pivote.getHijos()) {
                NodoBatallaNario padreEncontrado = buscarNodoxNombre(nombre, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado;
                }
            }
        }
        return null;
    }

    public BarcoPosicionado buscarBarcoxNombre(String nombre) {
        if (raiz != null) {
            return buscarBarcoxNombre(nombre, raiz);
        }
        return null;
    }

    private BarcoPosicionado buscarBarcoxNombre(String nombre, NodoBatallaNario pivote) {
        if (pivote.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return pivote.getDato();
        } else {
            for (NodoBatallaNario hijo : pivote.getHijos()) {
                NodoBatallaNario padreEncontrado = buscarNodoxNombre(nombre, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado.getDato();
                }
            }
        }
        return null;
    }

    public NodoBatallaNario buscarPadre(String nombre) {
        if (raiz.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return null;
        }
        return buscarPadre(nombre, raiz);
    }

    private NodoBatallaNario buscarPadre(String nombre, NodoBatallaNario pivote) {
        for (NodoBatallaNario hijo : pivote.getHijos()) {
            if (hijo.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
                return pivote;
            } else {
                NodoBatallaNario padreBuscado = buscarPadre(nombre, hijo);
                if (padreBuscado != null) {
                    return padreBuscado;
                }
            }
        }
        return null;
    }

    public void adicionarNodo(BarcoPosicionado dato, BarcoPosicionado padre) {
        if (raiz == null) {
            raiz = new NodoBatallaNario(dato);
        } else {
            NodoBatallaNario padreEncontrado = buscarNodoxNombre(padre.getTipobarco().getNombre());
            if (padreEncontrado != null) {
                padreEncontrado.getHijos().add(new NodoBatallaNario(dato));
            }

        }
        cantidadNodos++;
    }

    public void adicionarNodoxCodigo(BarcoPosicionado dato, BarcoPosicionado padre) {
        if (raiz == null) {
            raiz = new NodoBatallaNario(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;
    }

    public boolean adicionarNodoxCodigo(BarcoPosicionado dato, BarcoPosicionado padre, NodoBatallaNario pivote) {
        // boolean adicionado=false;
        if (pivote.getDato().getIdentificador() == padre.getIdentificador()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoBatallaNario(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoBatallaNario hijo : pivote.getHijos()) {
                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }

            }
        }
        return false;
    }

    public void reiniciarBarcos() {
        for (BarcoPosicionado listarBarcosPosicionado : listarBarcosPosicionados()) {
            listarBarcosPosicionado.setCoordenadas(null);
        }
        for (BarcoPosicionado listarBarcos : listarBarcos()) {
            listarBarcos.setCoordenadas(null);
        }
        for (BarcoPosicionado listarBarcosDestruidos : listarBarcosDestruidos()) {
            listarBarcosDestruidos.setCoordenadas(null);
        }
    }

    public List<BarcoPosicionado> listarBarcosPosicionados() {
        List<BarcoPosicionado> listaBarcos = new ArrayList<>();

        listarBarcosPosicionados(raiz, listaBarcos);
        return listaBarcos;
    }

    private void listarBarcosPosicionados(NodoBatallaNario nodo, List<BarcoPosicionado> barcos) {

        for (NodoBatallaNario agregar : nodo.getHijos()) {

            barcos.add(agregar.getDato());

            listarBarcosPosicionados(agregar, barcos);

        }
    }

    public List<BarcoPosicionado> listarBarcos() {
        List<BarcoPosicionado> listaBarcos = new ArrayList<>();

        listarBarcos(raiz, listaBarcos);
        return listaBarcos;
    }

    private void listarBarcos(NodoBatallaNario nodo, List<BarcoPosicionado> barcos) {

        for (NodoBatallaNario agregar : nodo.getHijos()) {
            if (agregar.getDato().getCoordenadas().isEmpty()) {
                barcos.add(agregar.getDato());
            }

            listarBarcos(agregar, barcos);

        }
    }

    public List<BarcoPosicionado> listarBarcosDestruidos() {
        List<BarcoPosicionado> listaBarcosDestruidos = new ArrayList<>();

        listarBarcosDestruidos(raiz, listaBarcosDestruidos);
        return listaBarcosDestruidos;
    }

    private void listarBarcosDestruidos(NodoBatallaNario nodo, List<BarcoPosicionado> barcos) {

        for (NodoBatallaNario agregar : nodo.getHijos()) {
            if (agregar.getDato().getEstado().getCodigo() == 2) {
                barcos.add(agregar.getDato());
            }

            listarBarcosDestruidos(agregar, barcos);

        }
    }

//METODO Instanciar
    public void instanciarCoordenadas(BarcoPosicionado barco, String direccion, int fila, int columna) throws BarcoExcepcion {
        if (barco.getCoordenadas().isEmpty()) {
            barco.setCoordenadas(new ArrayList<>());
        }
        
        if (direccion.equals("Vertical")) {
            for (int i = 0; i < barco.getTipobarco().getNroCasillas(); i++) {
                coordendas.add(new Coordenada((byte) (columna), (byte) (fila + i)));

//                    throw new BarcoExcepcion("Coordenadas Ocupadas");
            }
            int valorResta = verificarCoordenadaN(coordendas);

            if (valorResta == -1) {
                barco.getCoordenadas().addAll(coordendas);
                coordendas = new ArrayList<>();
            } else if (valorResta == 0) {
                coordendas = new ArrayList<>();
                throw new BarcoExcepcion("Coordenadas Ocupadas");
            } else {
                coordendas = new ArrayList<>();
                throw new BarcoExcepcion("-------El  Barco No  Debe Estar Junto a  Otro -----");
            }

        } else if (direccion.equals("Horizontal")) {
            for (int i = 0; i < barco.getTipobarco().getNroCasillas(); i++) {
                coordendas.add(new Coordenada((byte) (columna + i), (byte) (fila)));

            }

            int valorResta = verificarCoordenadaN(coordendas);

            if (valorResta == -1) {
                barco.getCoordenadas().addAll(coordendas);
                coordendas = new ArrayList<>();
            } else if (valorResta == 0) {
                coordendas = new ArrayList<>();
                throw new BarcoExcepcion("Coordenadas Ocupadas");
            } else {
                coordendas = new ArrayList<>();
                throw new BarcoExcepcion("-------El  Barco No  Debe Estar Junto a  Otro -----");
            }
        }

    }

    public int verificarCoordenadaN(List<Coordenada> coordenada) {
        int valorResta = verificarCoordenadaN(raiz, coordenada);
        return valorResta;

    }

    /**
     * *
     * El valor de la resta es 0 --> Si el barco esta sobre otro 1 --> Si esta
     * juto a otro barco
     *
     * @param pivote
     * @param coordenada
     * @return
     */
    private int verificarCoordenadaN(NodoBatallaNario pivote, List<Coordenada> coordenada) {
        int valorResta = -1; //se encontro la coordenada

        if (tieneCoordenadas(pivote.getDato())) {
            valorResta = verificarPosicionEspacios(pivote, coordenada);
        }
        if (valorResta == -1) {
            for (NodoBatallaNario hijo : pivote.getHijos()) {
                valorResta = verificarCoordenadaN(hijo, coordenada);
                if (valorResta > -1) {
                    break;
                }
            }
        }
        return valorResta;
    }

    /**
     * Verifica que las posiciones candidatas de un barco no esten ocupadas o
     * junto a otro barco
     *
     * @param pivote
     * @param coordenadasPosibles
     * @return
     */
    private int verificarPosicionEspacios(NodoBatallaNario pivote, List<Coordenada> coordenadasPosibles) {
        int valorResta = -1;
        List<Coordenada> posicionesBarco = pivote.dato.getCoordenadas();

        for (Coordenada posicionBarco : posicionesBarco) {
            for (Coordenada posicionCandidato : coordenadasPosibles) {
                int resta = restarCoordenadas(posicionBarco, posicionCandidato);
                if (resta == 1 || resta == 0) {
                    valorResta = resta;
                    break;
                }
            }
        }
        return valorResta;
    }

    /**
     * *
     * Resta las filas y columnas de dos coordenadas
     *
     * Se usa para verificar que exista el espacio entre dos barcos
     *
     * @param a Coordenada 1
     * @param b Coordenada 2
     * @return el valor abs de la diferencia
     */
    private int restarCoordenadas(Coordenada a, Coordenada b) {
        int respuesta = 0;
        respuesta = Math.abs(a.getFila() - b.getFila()) + Math.abs(a.getColumna() - b.getColumna());
        return respuesta;

    }

    public boolean tieneCoordenadas(BarcoPosicionado barcoPosicionado) {
        if (barcoPosicionado.getCoordenadas() != null) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "( " + raiz + ')';
    }

}
