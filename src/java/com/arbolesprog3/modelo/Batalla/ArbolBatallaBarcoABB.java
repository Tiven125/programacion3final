/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.Batalla;


import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.validador.TipoBarcoValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiven
 */
public class ArbolBatallaBarcoABB implements Serializable {

    private NodoBarcoABB raiz;
    private byte cantidadElementosBarcos;
    private int cantidadNodos;

    public ArbolBatallaBarcoABB() {
    }

    public NodoBarcoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBarcoABB raiz) {
        this.raiz = raiz;
    }

    public byte getCantidadElementosBarcos() {
        return cantidadElementosBarcos;
    }

    public void setCantidadElementosBarcos(byte cantidadElementosBarcos) {
        this.cantidadElementosBarcos = cantidadElementosBarcos;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public void adicionarNodo(TipoBarco dato) throws BarcoExcepcion {

        TipoBarcoValidador.validarDatos(dato);
        NodoBarcoABB nuevo = new NodoBarcoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo, raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoBarcoABB nuevo, NodoBarcoABB pivote)
            throws BarcoExcepcion {
        if (nuevo.getDato().getNombre().compareTo(pivote.getDato().getNombre()) == 0
                || nuevo.getDato().getNroCasillas() == pivote.getDato().getNroCasillas()) {
            throw new BarcoExcepcion("Ya existe un tipo de barco con  el nombre: \t"
                    + nuevo.getDato().getNombre() + " \t ó con el mismo numero de casillas: \t" + nuevo.getDato().getNroCasillas());
        } else if (nuevo.getDato().getNroCasillas() < pivote.getDato().getNroCasillas()) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }

    public List<TipoBarco> recorrerInOrden() {
        List<TipoBarco> listaBarcos = new ArrayList<>();
        recorrerInOrden(raiz, listaBarcos);
        return listaBarcos;
    }

    private void recorrerInOrden(NodoBarcoABB reco, List<TipoBarco> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listado);
        }
    }

    public int sumarCantidadBarcos() {

        return sumarInOrdenRecursivo(raiz);

    }

    private NodoBarcoABB buscarMin(NodoBarcoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return pivote;
    }

    private NodoBarcoABB buscarNodoMenor(NodoBarcoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    private NodoBarcoABB borrarBarco(NodoBarcoABB pivote, String entontrarBarco) {
        if (pivote == null) {
            return null;//<--Dato no encontrado	
        }
        int compara = pivote.getDato().getNombre().compareTo(entontrarBarco);
        if (compara > 0) {
            pivote.setIzquierda(borrarBarco(pivote.getIzquierda(), entontrarBarco));
        } else if (compara < 0) {
            pivote.setDerecha(borrarBarco(pivote.getDerecha(), entontrarBarco));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoBarcoABB cambiar = buscarNodoMenor(pivote.getDerecha());
                TipoBarco axiliar = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(axiliar);
                pivote.setDerecha(borrarBarco(pivote.getDerecha(),
                        entontrarBarco));

            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda()
                        : pivote.getDerecha();

            }
        }
        return pivote;
    }

    public NodoBarcoABB borrarDato(String entontrarBarco) {
        NodoBarcoABB nodoB = borrarBarco(raiz, entontrarBarco);
        this.setRaiz(nodoB);
        return nodoB;
    }

    


    
  

   

//Calcular tablero 
    public byte cantidadElementosBarcos() {
        cantidadElementosBarcos = 0;
        cantidadInOrden(raiz);
        return cantidadElementosBarcos;
    }

    private void cantidadInOrden(NodoBarcoABB reco) {
        if (reco != null) {
            cantidadInOrden(reco.getIzquierda());
            cantidadElementosBarcos += reco.getDato().getNroCasillas();
            cantidadInOrden(reco.getDerecha());
        }

    }

    public byte calcularTablero() {
        byte tamano = 0;
        if (cantidadElementosBarcos() <= 9) {
            tamano = 10;
        } else if (cantidadElementosBarcos() <= 20) {
            tamano = 20;
        } else {
            tamano = 30;
        }
        return tamano;
    }

    private int sumarPostOrden(NodoBarcoABB reco) {
        int sumarBarcos = 0;
        if (reco != null) {
            sumarBarcos += sumarPostOrden(reco.getIzquierda());
            sumarBarcos += sumarPostOrden(reco.getDerecha());
            sumarBarcos += reco.getDato().getCantidadJuego();
            return sumarBarcos;
        }
        return 0;
    }

    public int sumarInOrdenRecursivo(NodoBarcoABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivo(reco.getIzquierda());
            suma += reco.getDato().getCantidadJuego();
            suma += sumarInOrdenRecursivo(reco.getDerecha());
        }
        return suma;

    }
}
