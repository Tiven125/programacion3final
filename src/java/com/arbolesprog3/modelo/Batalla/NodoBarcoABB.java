/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.Batalla;

import java.io.Serializable;

/**
 *
 * @author tiven
 */
public class NodoBarcoABB implements Serializable {

    private TipoBarco dato;

    private NodoBarcoABB izquierda;
    private NodoBarcoABB derecha;

    public NodoBarcoABB(TipoBarco dato) {
        this.dato = dato;
    }

   

    public TipoBarco getDato() {
        return dato;
    }

    public void setDato(TipoBarco dato) {
        this.dato = dato;
    }

    public NodoBarcoABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoBarcoABB izquierda) {
        this.izquierda = izquierda;
    }

    public NodoBarcoABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoBarcoABB derecha) {
        this.derecha = derecha;
    }

   

}
