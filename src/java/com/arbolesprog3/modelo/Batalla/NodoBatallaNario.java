/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.Batalla;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiven
 */
public class NodoBatallaNario implements Serializable{
    BarcoPosicionado dato;
    List<NodoBatallaNario> hijos;

    public NodoBatallaNario() {
    }

    public NodoBatallaNario(BarcoPosicionado dato) {
        this.dato = dato;
        hijos = new ArrayList<>();
    }

    public BarcoPosicionado getDato() {
        return dato;
    }

    public void setDato(BarcoPosicionado dato) {
        this.dato = dato;
    }

    public List<NodoBatallaNario> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoBatallaNario> hijos) {
        this.hijos = hijos;
    }

    @Override
    public String toString() {
        return "{"  + dato.getTipobarco().getNombre() + '}';
    }
    
    
}
