/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.arbol.avl;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.abb.Celular;
import com.arbolesprog3.modelo.abb.NodoABB;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tiven
 */
public class ArbolAVL implements Serializable {

    private NodoAVL raiz;
    private int cantidadNodos;
    private int cantidadHojas;
    private String[] niveles;
    private int altura;

    //Constructor vacio
    public ArbolAVL() {
    }

    //Getters and setters
    public int getCantidadHojas() {
        return cantidadHojas;
    }

    public void setCantidadHojas(int cantidadHojas) {
        this.cantidadHojas = cantidadHojas;
    }

    public double getSumaPrecios() {
        return sumaPrecios;
    }

    public void setSumaPrecios(double sumaPrecios) {
        this.sumaPrecios = sumaPrecios;
    }

    public int getNumeroRamas() {
        return numeroRamas;
    }

    public void setNumeroRamas(int numeroRamas) {
        this.numeroRamas = numeroRamas;
    }

    public NodoAVL getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoAVL raiz) {
        this.raiz = raiz;
    }

    public void isLleno() throws CelularExcepcion {
        if (raiz == null) {
            throw new CelularExcepcion("El árbol está vacío");
        }
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public String[] getNiveles() {
        return niveles;
    }

    public void setNiveles(String[] niveles) {
        this.niveles = niveles;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    ///Adicionar en el árbol
    public void adicionarNodo(Celular dato, NodoAVL ubicacion) throws CelularExcepcion {
        if (raiz == null) {
            raiz = new NodoAVL(dato);
            cantidadNodos++;

        } else {

            if (dato.getImei().compareTo(ubicacion.getDato().getImei()) < 0) {
                if (ubicacion.getIzquierda() == null) {
                    ubicacion.setIzquierda(new NodoAVL(dato));
                    cantidadNodos++;

                } else {
                    adicionarNodo(dato, ubicacion.getIzquierda());
                }
            } else if (dato.getImei().compareTo(ubicacion.getDato().getImei()) > 0) {
                if (ubicacion.getDerecha() == null) {
                    ubicacion.setDerecha(new NodoAVL(dato));
                    cantidadNodos++;
                } else {
                    adicionarNodo(dato, ubicacion.getDerecha());
                }
            } else {
                throw new CelularExcepcion("No se puede repetir");
            }

            ubicacion.actualizarAltura();
            balancear(ubicacion);

        }
    }
    
    
     public void rotarSimple(NodoAVL principal, boolean sentido) {
        NodoAVL temp = new NodoAVL(principal.getDato());
        if (!sentido) {
            principal.setDato(principal.getIzquierda().getDato());
            principal.setDerecha(new NodoAVL(temp.getDato()));
            principal.setIzquierda(principal.getIzquierda().getIzquierda());
            principal.getIzquierda().actualizarAltura();
            principal.actualizarAltura();
        } else {
            principal.setDato(principal.getDerecha().getDato());
            principal.setIzquierda(new NodoAVL(temp.getDato()));
            principal.setDerecha(principal.getDerecha().getDerecha());
            principal.getDerecha().actualizarAltura();
            principal.actualizarAltura();

        }
    }

    public void rotarSimpleNuevo(NodoAVL princ, boolean sentido) throws CelularExcepcion {

        if (!sentido) {
            if (princ.getDerecha() != null) {
                NodoAVL nodo = princ.getDerecha();
                princ.setDerecha(new NodoAVL(princ.getDato()));
                princ.getDerecha().setDerecha(nodo);
            } else {
                princ.setDerecha(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getIzquierda().getDato());
            if (princ.getIzquierda().getDerecha() != null) {
                NodoAVL nodo = princ.getIzquierda().getDerecha();
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
                adicionarNodo(nodo.getDato(), princ);
//                princ.getIzquierda().setDerecha(nodo);
            } else {
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
            }
            princ.getDerecha().actualizarAltura();
        } else {
            if (princ.getIzquierda() != null) {
                NodoAVL nodo = princ.getIzquierda();
                princ.setIzquierda(new NodoAVL(princ.getDato()));
                princ.getIzquierda().setIzquierda(nodo);
            } else {
                princ.setIzquierda(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getDerecha().getDato());
            if (princ.getDerecha().getIzquierda() != null) {
                NodoAVL nodo = princ.getDerecha().getIzquierda();
                princ.setDerecha(princ.getDerecha().getDerecha());
                adicionarNodo(nodo.getDato(), princ);
//
            } else {
                princ.setDerecha(princ.getDerecha().getDerecha());
            }
            princ.getIzquierda().actualizarAltura();
        }
        princ.actualizarAltura();
    }

    
    
    public void balancear(NodoAVL principal) throws CelularExcepcion {
        if (!principal.esVacio()) {
            int fe = principal.obtenerFactorEquilibrio();
            boolean signo = true;
            if (fe < 0) {
                signo = false;
                fe = fe * -1;
            }
            if (fe >= 2) {
                //Esta desequilibrado
                //hacia donde
                if (signo) {
                    //Desequilibrio a la derecha
                    //Valido desequilibrio simple a la izq
                    if (principal.getDerecha().obtenerFactorEquilibrio() > 0) {
                        //Desequilibrio simple - Rotacion simple
                        rotarSimpleNuevo(principal, signo);

                    } else {
                        //Desequilibrio doble - Rotación doble
                        rotarSimpleNuevo(principal.getDerecha(), false);
                        rotarSimpleNuevo(principal, true);
                    }
                } else {
                    //Desequilibrio a la izquierda
                    //Valido desequilibrio simple a la izq
                    if (principal.getIzquierda().obtenerFactorEquilibrio() < 0) {
                        rotarSimpleNuevo(principal, signo);
                    } else {
                        //Tengo un zig zag
                        //rotar doble
                        rotarSimpleNuevo(principal.getIzquierda(), true);
                        rotarSimpleNuevo(principal, false);
                    }
                }
            }
        }

    }

    public List<Celular> recorrerInOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerInOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerInOrden(NodoAVL reco, List<Celular> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPreOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerPreOrden(NodoAVL reco, List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(), listado);
            recorrerPreOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPostOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoAVL reco, List<Celular> listado) {
        if (reco != null) {

            recorrerPostOrden(reco.getIzquierda(), listado);
            recorrerPostOrden(reco.getDerecha(), listado);
            listado.add(reco.getDato());
        }
    }

    public boolean esVacio() {
        return raiz == null;
    }

    public int sumarInOrdenRecursivo(NodoAVL reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivo(reco.getIzquierda());
            suma += reco.getDato().getCantidad();
            suma += sumarInOrdenRecursivo(reco.getDerecha());
        }
        return suma;

    }
    private double sumaPrecios;

    public double sumarPrecios() {
        sumaPrecios = 0;
        sumarInOrden(raiz);
        return sumaPrecios;
    }

    private void sumarInOrden(NodoAVL reco) {
        if (reco != null) {
            sumarInOrden(reco.getIzquierda());
            sumaPrecios += reco.getDato().getPrecio();
            sumarInOrden(reco.getDerecha());
        }

    }

    public double calcularPromedioPrecios() {
        return sumarPrecios() / (double) cantidadNodos;
    }

    // NUEVOS  METODOS
    public List<Celular> buscarOperador() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerInOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private List<NodoAVL> buscarOperador(NodoAVL raiz, String operador) {

        List<NodoAVL> listaOperador = new ArrayList<>();
        if (raiz != null) {
            if (operador.equalsIgnoreCase(raiz.getDato().getOperador().getNombre())) {
                buscarOperador(raiz.getIzquierda(), operador);
                buscarOperador(raiz.getDerecha(), operador);
            } else {
                listaOperador.add(raiz);
            }

        }

        return listaOperador;
    }

    //cantidad nodos hoja
    public String cantidadNodosHoja() {
        cantidadHojas = 0;
        cantidadNodosHoja(raiz);
        return "" + cantidadHojas;
    }

    private void cantidadNodosHoja(NodoAVL reco) {
        if (reco != null) {
            if (reco.getIzquierda() == null && reco.getDerecha() == null) {
                cantidadHojas++;
            }
            cantidadNodosHoja(reco.getIzquierda());
            cantidadNodosHoja(reco.getDerecha());
        }
    }

    //mayor Valor
    public String valorMayor() {
        NodoAVL reco = raiz;
        if (raiz != null) {
            while (reco.getDerecha() != null) {
                reco = reco.getDerecha();
            }
        }
        return ("" + reco.getDato());
    }

    //Valor menor
    public String valorMenor() {
        NodoAVL reco = raiz;
        if (raiz != null) {

            while (reco.getIzquierda() != null) {
                reco = reco.getIzquierda();
            }
        }
        return ("" + reco.getDato());
    }

    //hojas
    public ArrayList getHojas() {
        ArrayList l = new ArrayList();
        getHojas(this.raiz, l);
        return (l);
    }

    private void getHojas(NodoAVL pivote, ArrayList l) {
        if (pivote != null) {
            if (this.esHoja(pivote)) {
                l.add(pivote.getDato());
            }
            getHojas(pivote.getIzquierda(), l);
            getHojas(pivote.getDerecha(), l);
        }

    }

    protected boolean esHoja(NodoAVL pivote) {
        return (pivote != null && pivote.getIzquierda() == null && pivote.getDerecha() == null);
    }

//    Rama Mayores
    int numeroRamas = 0;

    public ArrayList<String> ObtenerRamamayor() {
        obtenernumeroRamas(this.raiz, 0);
        return ObtenerRamamayor(this.raiz, 0, "", new ArrayList<String>());
    }

    public void obtenernumeroRamas(NodoAVL pivote, int contador) {
        if (pivote != null) {
            contador++;
            obtenernumeroRamas(pivote.getIzquierda(), contador);
            obtenernumeroRamas(pivote.getDerecha(), contador);
        }
        if (contador > this.numeroRamas) {
            this.numeroRamas = contador;
        }
    }

    public ArrayList<String> ObtenerRamamayor(NodoAVL pivote, int contador, String dato, ArrayList lista) {
        if (pivote != null) {
            dato += pivote.getDato() + ",";
            contador++;
            lista = ObtenerRamamayor(pivote.getIzquierda(), contador, dato, lista);
            lista = ObtenerRamamayor(pivote.getDerecha(), contador, dato, lista);

            if (contador == this.numeroRamas) {
                lista.add(dato);
            }
        }
        return lista;
    }

    private NodoAVL buscarNodoMenor(NodoAVL pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    public Celular buscarCelularMenor() {
        return buscarCelularMenor(raiz);
    }

    private Celular buscarCelularMenor(NodoAVL pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());

        return (pivote.getDato());
    }

    //Borrar menor
    public Celular borrarMenor() {
        NodoAVL reco = raiz.getIzquierda();
        if (raiz != null) {
            if (raiz.getIzquierda() == null) {
                raiz = raiz.getDerecha();
            } else {
                NodoAVL anterior = raiz;
                reco = raiz.getIzquierda();
                while (reco.getIzquierda() != null) {
                    anterior = reco;
                    reco = reco.getIzquierda();
                }

                anterior.setIzquierda(reco.getDerecha());
            }
        }
        return reco.getDato();
    }

    //borrar mayor
    public Celular borrarMayor() {
        NodoAVL reco = raiz.getIzquierda();
        if (raiz != null) {
            if (raiz.getDerecha() == null) {
                raiz = raiz.getIzquierda();
            } else {
                NodoAVL anterior = raiz;
                reco = raiz.getDerecha();
                while (reco.getDerecha() != null) {
                    anterior = reco;
                    reco = reco.getDerecha();
                }

                anterior.setDerecha(reco.getIzquierda());
            }
        }
        return reco.getDato();
    }

//    Eliminar por Posicion
    private NodoAVL borrarCelular(NodoAVL pivote, String encontrarImei) {
        if (pivote == null) {
            return null;//<--Dato no encontrado	
        }
        int compara = pivote.getDato().getImei().compareTo(encontrarImei);
        if (compara > 0) {
            pivote.setIzquierda(borrarCelular(pivote.getIzquierda(), encontrarImei));
        } else if (compara < 0) {
            pivote.setDerecha(borrarCelular(pivote.getDerecha(), encontrarImei));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoAVL cambiar = buscarNodoMenor(pivote.getDerecha());
                Celular axiliar = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(axiliar);
                pivote.setDerecha(borrarCelular(pivote.getDerecha(),
                        encontrarImei));

            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda()
                        : pivote.getDerecha();

            }
        }
        return pivote;
    }

    public NodoAVL borrarDato(String BorrarImei) {
        NodoAVL nodoB = borrarCelular(raiz, BorrarImei);
        this.setRaiz(nodoB);
        return nodoB;
    }

    
    // buscar Padre
    
    public String buscarPadre(String info) {
        if (info.equals("") || this.raiz == null) {
            return "";
        }
        NodoAVL padreEncontrado = buscarPadre(this.raiz, info);
        if (padreEncontrado == null) {
            return "";
        }
        return (padreEncontrado.getDato().getImei());
    }

    private NodoAVL buscarPadre(NodoAVL padreEncontrado, String info) {
        if (padreEncontrado == null) {
            return null;
        }
        if ((padreEncontrado.getIzquierda() != null && padreEncontrado.getIzquierda().getDato().getImei().equals(info)) || (padreEncontrado.getDerecha() != null && padreEncontrado.getDerecha().getDato().getImei().equals(info))) {
            return padreEncontrado;
        }
        NodoAVL y = buscarPadre(padreEncontrado.getIzquierda(), info);
        if (y == null) {
            return (buscarPadre(padreEncontrado.getDerecha(), info));
        } else {
            return (y);
        }
    }

    //Buscar
    public boolean buscar(String encontrarImei) {
        return (buscar(this.raiz, encontrarImei));

    }

    private boolean buscar(NodoAVL pivote, String encontrarImei) {
        if (pivote == null) {
            return (false);
        }
        int compara = ((Comparable) pivote.getDato().getImei()).compareTo(encontrarImei);
        if (compara > 0) {
            return (buscar(pivote.getIzquierda(), encontrarImei));
        } else if (compara < 0) {
            return (buscar(pivote.getDerecha(), encontrarImei));
        } else {
            return (true);
        }
    }

    //eliminar hojas
    public void podar() {
        podar(this.raiz);
    }

    private void podar(NodoAVL x) {
        if (x == null) {
            return;
        }
        if (this.esHoja(x.getIzquierda())) {
            x.setIzquierda(null);
        }
        if (this.esHoja(x.getDerecha())) {
            x.setDerecha(null);
        }
        podar(x.getIzquierda());
        podar(x.getIzquierda());
    }

//    
}
