/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.arbol.nario;


import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.abb.Celular;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author tiven
 */
public class ArbolEneario implements Serializable {

    NodoEneario raiz;
    int cantidadNodos;

    public ArbolEneario() {
    }

    public NodoEneario getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoEneario raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public boolean esHoja(NodoEneario nodo) {
        if (nodo.equals(raiz)) {
            return false;

        }

        return nodo.getHijos() == null;

    }

    
     public void adicionarNodo(Celular dato, String imeiPadre) throws CelularExcepcion {
        if (raiz == null) {
            raiz = new NodoEneario(dato);

        } else {
            adicionarNodo(dato, imeiPadre, raiz);

        }
        cantidadNodos++;

    }

    private boolean adicionarNodo(Celular dato, String imeiPadre, NodoEneario pivote) throws CelularExcepcion {
        // boolean adicionado=false;
        if (pivote.getDato().getImei().
                compareTo(imeiPadre) == 0) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoEneario(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoEneario hijo : pivote.getHijos()) {
                if (adicionarNodo(dato, imeiPadre, hijo))
                    break;
            }
        }
        return false;
    }
    
    
    
    

    public NodoEneario buscarPadre(String imei) {
        if (raiz.getDato().getImei().compareTo(imei) == 0) {
            return null;
        }
        return buscarPadre(imei, raiz);
    }

    private NodoEneario buscarPadre(String imei, NodoEneario pivote) {
        for (NodoEneario hijo : pivote.getHijos()) {
            if (hijo.getDato().getImei().compareTo(imei) == 0) {
                return pivote;
            } else {
                NodoEneario padreBuscado = buscarPadre(imei, hijo);
                if (padreBuscado != null) {
                    return padreBuscado;
                }
            }
        }
        return null;
    }

    
    
    public void adicionarNodoxCodigo(Celular dato, Celular padre)  {
        if (raiz == null) {
            raiz = new NodoEneario(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;
    }

    public boolean adicionarNodoxCodigo(Celular dato, Celular padre, NodoEneario pivote)  {
        // boolean adicionado=false;
        if (pivote.getDato().getCodigo() == padre.getCodigo()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoEneario(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoEneario hijo : pivote.getHijos()) {
                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }

            }
        }
        return false;
    }

    public int obtenerTamaño(NodoEneario nodo, ArbolEneario arbol) {
        if (nodo != null) {
            cantidadNodos++;
            for (NodoEneario tmp : nodo.hijos) {
                obtenerTamaño(tmp, arbol);

            }
            return cantidadNodos;
        }
        return 0;
    }

    public List<Celular> listarCelularesN() {
        List<Celular> listaCelulares = new ArrayList<>();
        listaCelulares.add(raiz.getDato());
        listarCelularesN(raiz, listaCelulares);
        return listaCelulares;
    }

    private void listarCelularesN(NodoEneario nodo, List<Celular> celulares) {
        for (NodoEneario agregar : nodo.getHijos()) {
            celulares.add(agregar.getDato());
            listarCelularesN(agregar, celulares);
        }
    }
    
     public boolean eliminarNodo(String id) {
        NodoEneario padre = buscarPadre(id);
        if (padre != null) {
            int cont = 0;
            for (NodoEneario hijo : padre.getHijos()) {
                if (hijo.getDato().getImei().compareTo(id) == 0) {
                    padre.getHijos().addAll(hijo.getHijos());

                    break;
                }
                cont++;
            }
            padre.getHijos().remove(cont);
            cantidadNodos--;
            return true;
        }
        return false;
    }
}