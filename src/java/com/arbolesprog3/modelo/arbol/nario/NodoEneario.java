/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo.arbol.nario;

import com.arbolesprog3.modelo.abb.Celular;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class NodoEneario implements Serializable{
    
    Celular dato;
    List<NodoEneario> hijos;

    public NodoEneario() {
    }

    public NodoEneario(Celular dato) {
        this.dato = dato;
        hijos = new ArrayList<>();
    }

    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public List<NodoEneario> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoEneario> hijos) {
        this.hijos = hijos;
    }

    
    
    
    
}
