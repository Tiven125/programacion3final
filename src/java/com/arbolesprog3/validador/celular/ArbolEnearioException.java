/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.validador.celular;

/**
 *
 * @author usuario
 */
public class ArbolEnearioException extends Exception{

    public ArbolEnearioException() {
    }

    public ArbolEnearioException(String message) {
        super(message);
    }

    public ArbolEnearioException(String message, Throwable cause) {
        super(message, cause);
    }

    public ArbolEnearioException(Throwable cause) {
        super(cause);
    }
    
}

