
package com.arbolesprog3.excepcion;


public class BarcoExcepcion extends Exception{

    public BarcoExcepcion() {
    }

    public BarcoExcepcion(String message) {
        super(message);
    }

    public BarcoExcepcion(String message, Throwable cause) {
        super(message, cause);
    }

    public BarcoExcepcion(Throwable cause) {
        super(cause);
    }
    
    
}
