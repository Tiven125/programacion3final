/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador.Batalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.modelo.Batalla.ArbolBatallaBarcoABB;
import com.arbolesprog3.modelo.Batalla.NodoBarcoABB;
import com.arbolesprog3.modelo.Batalla.TipoBarco;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;

/**
 *
 * @author tiven
 */
@Named(value = "controladorBarcosBatallaNaval")
@SessionScoped
public class ControladorBarcosBatallaNaval implements Serializable {

    private TipoBarco barco = new TipoBarco();
    private NodoBarcoABB nodoBatalla;
    private ArbolBatallaBarcoABB arbolBB = new ArbolBatallaBarcoABB();
    private byte cantidadElementos;
    private String encontrarBarco = "";

    private boolean verRegistrar = false;
    private boolean ocultarPanel = true;
    private Boolean deshabilitarTableros = true;

    /**
     * Creates a new instance of ConroladorBatallaNaval
     */
    public ControladorBarcosBatallaNaval() {
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }

    public NodoBarcoABB getNodoBatalla() {
        return nodoBatalla;
    }

    public void setNodoBatalla(NodoBarcoABB nodoBatalla) {
        this.nodoBatalla = nodoBatalla;
    }

    public Boolean getDeshabilitarTableros() {
        return deshabilitarTableros;
    }

    public void setDeshabilitarTableros(Boolean deshabilitarTableros) {
        this.deshabilitarTableros = deshabilitarTableros;
    }

    public ArbolBatallaBarcoABB getArbolBB() {
        return arbolBB;
    }

    public void setArbolBB(ArbolBatallaBarcoABB arbolBB) {
        this.arbolBB = arbolBB;
    }

    public String getEncontrarBarco() {
        return encontrarBarco;
    }

    public void setEncontrarBarco(String encontrarBarco) {
        this.encontrarBarco = encontrarBarco;
    }

   
    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public boolean isOcultarPanel() {
        return ocultarPanel;
    }

    public void setOcultarPanel(boolean ocultarPanel) {
        this.ocultarPanel = ocultarPanel;
    }

    public byte getCantidadElementos() {
        return cantidadElementos;
    }

    public void setCantidadElementos(byte cantidadElementos) {
        this.cantidadElementos = cantidadElementos;
    }

    
    
    @PostConstruct
    private void iniciar() {

        try {
            arbolBB.adicionarNodo(new TipoBarco("Buque", (byte) 1, (byte) 4));
//           

        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

        habilitarTableros();
    }

//    
    public void guardarTipoBarco() {
        try {
            arbolBB.adicionarNodo(barco);
            barco = new TipoBarco();
            verRegistrar = false;
            ocultarPanel = true;
            deshabilitarTableros = false;
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        ocultarPanel = false;

    }

    public void cancelarGuardado() {
        verRegistrar = false;
        ocultarPanel = true;

    }

    public void borrarBarco() {
        arbolBB.borrarDato(getEncontrarBarco());
        JsfUtil.addSuccessMessage("Barco elimado Con  Exito");

    }

    public void habilitarTableros() {
        if (arbolBB.getRaiz() != null) {
            deshabilitarTableros = false;
        } else {
            deshabilitarTableros = true;
        }
    }

}
