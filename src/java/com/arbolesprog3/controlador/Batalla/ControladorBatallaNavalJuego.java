/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador.Batalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.modelo.Batalla.ArbolBatallaBarcoABB;
import com.arbolesprog3.modelo.Batalla.ArbolBatallaBarcoNario;
import com.arbolesprog3.modelo.Batalla.BarcoPosicionado;
import com.arbolesprog3.modelo.Batalla.Coordenada;
import com.arbolesprog3.modelo.Batalla.Disparo;
import com.arbolesprog3.modelo.Batalla.NodoBarcoABB;
import com.arbolesprog3.modelo.Batalla.NodoBatallaNario;
import com.arbolesprog3.modelo.Batalla.Rol;
import com.arbolesprog3.modelo.Batalla.TipoBarco;
import com.arbolesprog3.modelo.Batalla.Usuario;
import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author tiven
 */
@Named(value = "controladorBatallaNavalJuego")
@ApplicationScoped
public class ControladorBatallaNavalJuego implements Serializable {

    
    private BarcoPosicionado barcoPos = new BarcoPosicionado();
    private ArbolBatallaBarcoNario arbolNBatallaJugador1 = new ArbolBatallaBarcoNario();
    private ArbolBatallaBarcoNario arbolNBatallaJugador2 = new ArbolBatallaBarcoNario();
    private ArbolBatallaBarcoABB arbolBinario = new ArbolBatallaBarcoABB();
    private ControladorBarcosBatallaNaval contAbb = (ControladorBarcosBatallaNaval) FacesUtils.
            getManagedBean("controladorBarcosBatallaNaval");
    private NodoBatallaNario nodoN;
    private NodoBarcoABB nodoABB;
    private Usuario administrador;
    private Usuario jugador1;
    private Usuario jugador2;
    private List<Rol> roles;
    private List<Disparo> disparosJugador1 = new ArrayList<>();
    private List<Disparo> disparosJugador2 = new ArrayList<>();
    private Coordenada coordenada = new Coordenada();
    private Rol rol = new Rol();
    private int fila;
    private int columna;
    private int fila1 = 0;
    private int columna1 = 0;
    private int tablero1;
    private int tablero2;
    private String direccion = "seleccione";
    private String estilo;
    private String estado;
    private boolean deshabilitarAsignarJugador1 = true;
    private boolean deshabilitarAsignarJugador2 = true;
    private boolean habilitarTableros = false;
    private boolean turno1 = false;
    private boolean turno2 = true;
    private boolean listo1 = false;
    private boolean listo2 = false;
    private boolean reiniciar = true;

    /**
     * Creates a new instance of ControladorJuego
     */
    public ControladorBatallaNavalJuego() {
    }

    public BarcoPosicionado getBarcoPos() {
        return barcoPos;
    }

    public void setBarcoPos(BarcoPosicionado barcoPos) {
        this.barcoPos = barcoPos;
    }

    public ArbolBatallaBarcoNario getArbolNBatallaJugador1() {
        return arbolNBatallaJugador1;
    }

    public void setArbolNBatallaJugador1(ArbolBatallaBarcoNario arbolNBatallaJugador1) {
        this.arbolNBatallaJugador1 = arbolNBatallaJugador1;
    }

    public ArbolBatallaBarcoNario getArbolNBatallaJugador2() {
        return arbolNBatallaJugador2;
    }

    public void setArbolNBatallaJugador2(ArbolBatallaBarcoNario arbolNBatallaJugador2) {
        this.arbolNBatallaJugador2 = arbolNBatallaJugador2;
    }

    public ArbolBatallaBarcoABB getArbolBinario() {
        return arbolBinario;
    }

    public void setArbolBinario(ArbolBatallaBarcoABB arbolBinario) {
        this.arbolBinario = arbolBinario;
    }

    public NodoBatallaNario getNodoN() {
        return nodoN;
    }

    public void setNodoN(NodoBatallaNario nodoN) {
        this.nodoN = nodoN;
    }

    public NodoBarcoABB getNodoABB() {
        return nodoABB;
    }

    public void setNodoABB(NodoBarcoABB nodoABB) {
        this.nodoABB = nodoABB;
    }

    public Usuario getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Usuario administrador) {
        this.administrador = administrador;
    }

    public Usuario getJugador1() {
        return jugador1;
    }

    public void setJugador1(Usuario jugador1) {
        this.jugador1 = jugador1;
    }

    public Usuario getJugador2() {
        return jugador2;
    }

    public void setJugador2(Usuario jugador2) {
        this.jugador2 = jugador2;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public ControladorBarcosBatallaNaval getContAbb() {
        return contAbb;
    }

    public void setContAbb(ControladorBarcosBatallaNaval contAbb) {
        this.contAbb = contAbb;
    }

    public int getTablero1() {
        return tablero1;
    }

    public void setTablero1(int tablero1) {
        this.tablero1 = tablero1;
    }

    public int getTablero2() {
        return tablero2;
    }

    public void setTablero2(int tablero2) {
        this.tablero2 = tablero2;
    }

    public Coordenada getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(Coordenada coordenada) {
        this.coordenada = coordenada;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Disparo> getDisparosJugador1() {
        return disparosJugador1;
    }

    public void setDisparosJugador1(List<Disparo> disparosJugador1) {
        this.disparosJugador1 = disparosJugador1;
    }

    public List<Disparo> getDisparosJugador2() {
        return disparosJugador2;
    }

    public void setDisparosJugador2(List<Disparo> disparosJugador2) {
        this.disparosJugador2 = disparosJugador2;
    }

    public boolean isDeshabilitarAsignarJugador1() {
        return deshabilitarAsignarJugador1;
    }

    public void setDeshabilitarAsignarJugador1(boolean deshabilitarAsignarJugador1) {
        this.deshabilitarAsignarJugador1 = deshabilitarAsignarJugador1;
    }

    public boolean isTurno1() {
        return turno1;
    }

    public void setTurno1(boolean turno1) {
        this.turno1 = turno1;
    }

    public boolean isTurno2() {
        return turno2;
    }

    public void setTurno2(boolean turno2) {
        this.turno2 = turno2;
    }

    public int getFila1() {
        return fila1;
    }

    public void setFila1(int fila1) {
        this.fila1 = fila1;
    }

    public int getColumna1() {
        return columna1;
    }

    public void setColumna1(int columna1) {
        this.columna1 = columna1;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public boolean isDeshabilitarAsignarJugador2() {
        return deshabilitarAsignarJugador2;
    }

    public void setDeshabilitarAsignarJugador2(boolean deshabilitarAsignarJugador2) {
        this.deshabilitarAsignarJugador2 = deshabilitarAsignarJugador2;
    }

    public boolean isHabilitarTableros() {
        return habilitarTableros;
    }

    public void setHabilitarTableros(boolean habilitarTableros) {
        this.habilitarTableros = habilitarTableros;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public boolean isListo1() {
        return listo1;
    }

    public void setListo1(boolean listo1) {
        this.listo1 = listo1;
    }

    public boolean isListo2() {
        return listo2;
    }

    public void setListo2(boolean listo2) {
        this.listo2 = listo2;
    }

    public boolean isReiniciar() {
        return reiniciar;
    }

    public void setReiniciar(boolean reiniciar) {
        this.reiniciar = reiniciar;
    }

    @PostConstruct
    private void iniciar() {

        llenarRol();

        administrador = new Usuario("Tiven125", "12345", new Rol((byte) 0, "Administrador"));
        jugador1 = new Usuario("Pita", "12345", roles.get(0));
        jugador2 = new Usuario("Niña", "12345", roles.get(1));

    }

    public int crearTableroJugador1() {
        if (arbolNBatallaJugador1.getCantidadNodos() <= 10 && arbolNBatallaJugador1.getCantidadNodos() != 0) {
            tablero1 = 10;

        } else if (arbolNBatallaJugador1.getCantidadNodos() > 10 && arbolNBatallaJugador1.getCantidadNodos() <= 20) {
            tablero1 = 20;

        } else if (arbolNBatallaJugador1.getCantidadNodos() > 20 && arbolNBatallaJugador1.getCantidadNodos() <= 30) {
            tablero1 = 30;

        }
        return tablero1;
    }

    public int crearTableroJugador2() {

        if (arbolNBatallaJugador2.getCantidadNodos() <= 10 && arbolNBatallaJugador2.getCantidadNodos() != 0) {
            tablero2 = 10;

        } else if (arbolNBatallaJugador2.getCantidadNodos() > 10 && arbolNBatallaJugador2.getCantidadNodos() <= 20) {
            tablero2 = 20;

        } else if (arbolNBatallaJugador2.getCantidadNodos() > 20 && arbolNBatallaJugador2.getCantidadNodos() <= 30) {
            tablero2 = 30;

        }
        return tablero2;
    }

    public void llenarRol() {
        roles = new ArrayList<>();
        roles.add(new Rol((byte) 1, "Jugador 1"));
        roles.add(new Rol((byte) 2, "Jugador 2"));

    }

    private void cargarArbolJugador1() throws BarcoExcepcion {

        //Algoritmo
        arbolNBatallaJugador1.adicionarNodo(new BarcoPosicionado(new TipoBarco(jugador1.getCorreo(), (byte) 0, (byte) 0)), null);

        List<BarcoPosicionado> padres = new ArrayList<>();
        padres.add(arbolNBatallaJugador1.getRaiz().getDato());
        adicionarPreOrdenJugador1(contAbb.getArbolBB().getRaiz(), padres, 0);

    }

    private void cargarArbolJugador2() throws BarcoExcepcion {

        //Algoritmo
        arbolNBatallaJugador2.adicionarNodo(new BarcoPosicionado(new TipoBarco(jugador2.getCorreo(), (byte) 0, (byte) 0)), null);

        List<BarcoPosicionado> padres = new ArrayList<>();
        padres.add(arbolNBatallaJugador2.getRaiz().getDato());
        adicionarPreOrdenJugador2(contAbb.getArbolBB().getRaiz(), padres, 0);

    }

    private void adicionarPreOrdenJugador1(NodoBarcoABB reco, List<BarcoPosicionado> padres, int contizq) throws BarcoExcepcion {
        if (reco != null) {
            List<BarcoPosicionado> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                BarcoPosicionado barcoNuevo = new BarcoPosicionado(reco.getDato());
                barcoNuevo.setIdentificador(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolNBatallaJugador1.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenJugador1(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + contAbb.getArbolBB().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrdenJugador1(reco.getDerecha(), padresNuevos, contizq);
        }
    }

    private void adicionarPreOrdenJugador2(NodoBarcoABB reco, List<BarcoPosicionado> padres, int contizq) throws BarcoExcepcion {
        if (reco != null) {
            List<BarcoPosicionado> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                BarcoPosicionado barcoNuevo = new BarcoPosicionado(reco.getDato());
                barcoNuevo.setIdentificador(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolNBatallaJugador2.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenJugador2(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + contAbb.getArbolBB().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrdenJugador2(reco.getDerecha(), padresNuevos, contizq);
        }
    }

   

   

   

    public void generarBarcosJuego() {

        try {

            cargarArbolJugador1();
            cargarArbolJugador2();
            JsfUtil.addSuccessMessage("Barcos Generados  con exito");
        } catch (BarcoExcepcion ex) {
            Logger.getLogger(ControladorBatallaNavalJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        contAbb.setDeshabilitarTableros(true);
    }

    public void asignarCoordenadasjugador1MenuContext(BarcoPosicionado barcopos) throws BarcoExcepcion {

        if (validarCoordenadaOcupadaJugador1(fila, columna)) {

            JsfUtil.addErrorMessage("Coordenadas Ocupadas");

        } else if (direccion.equals("Vertical") && (fila + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero1) {
            JsfUtil.addErrorMessage("Filas Fuera de Rango");

        } else if (direccion.equals("Horizontal") && (columna + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero1) {
            JsfUtil.addErrorMessage("columnas Fuera de Rango");
        } else {
            try {
                arbolNBatallaJugador1.instanciarCoordenadas(barcopos, direccion, fila, columna);
            } catch (BarcoExcepcion ex) {
                JsfUtil.addErrorMessage(ex.getMessage());
            }
        }

    }


    public void asignarCoordenadasjugador2MenuContext(BarcoPosicionado barcopos) {

        if (validarCoordenadaOcupadaJugador2(fila, columna)) {

            JsfUtil.addErrorMessage("Coordenadas Ocupadas");

        } else if (direccion.equals("Vertical") && (fila + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero2) {
            JsfUtil.addErrorMessage("Filas Fuera de Rango");

        } else if (direccion.equals("Horizontal") && (columna + barcopos.getTipobarco().getNroCasillas()) - 1 > tablero2) {
            JsfUtil.addErrorMessage("columnas Fuera de Rango");
        } else {
            try {
                arbolNBatallaJugador2.instanciarCoordenadas(barcopos, direccion, fila, columna);
            } catch (BarcoExcepcion ex) {
                JsfUtil.addErrorMessage(ex.getMessage());
            }
        }

    }

    public boolean verififcarDisparoJugador1(int fila, int columna) {
        if (disparosJugador2 != null) {
            for (Disparo disp : disparosJugador2) {
                if (disp.getFila() == fila && disp.getColumna() == columna) {
                    return true;
                }
            }

        }

        return false;
    }

    public void dispararJug1() {
        if (verififcarDisparoJugador2(fila1, columna1)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Ya disparó en estas coordenadas"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                    "Has disparado en la fila: " + fila1 + " columna: " + columna1));
            disparosJugador1.add(new Disparo(fila1, columna1));
            for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
                if (miBarquito.validarInpacto(fila1, columna1)) {
                    if (miBarquito.verificarEstadoBarco()) {
                        if (miBarquito.getEstado().getDescripcion().equals("Destruido")) {
                            estado = "Destruido";
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                                    "Haz destruido el barco: " + miBarquito.getTipobarco().getNombre()));
                        } else if (miBarquito.getEstado().getDescripcion().equals("Tocado")) {
                            estado = "Tocado";
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                                    "Haz acertado! "));
                        }
                    }
                    verificarGanador();
                    return;
                }
            }
            estado = "Agua";
            turno1 = true;
            turno2 = false;
            manejarTurnos();
        }
    }

    public boolean verififcarDisparoJugador2(int fila, int columna) {
        if (disparosJugador1 != null) {
            for (Disparo disp : disparosJugador1) {
                if (disp.getFila() == fila && disp.getColumna() == columna) {
                    return true;
                }
            }

        }

        return false;
    }

    public void dispararJug2() {
        if (verififcarDisparoJugador1(fila1, columna1)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Ya disparó en estas coordenadas"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                    "Has disparado en la fila: " + fila1 + " columna: " + columna1));
            disparosJugador2.add(new Disparo(fila1, columna1));
            for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
                if (miBarquito.validarInpacto(fila1, columna1)) {
                    if (miBarquito.verificarEstadoBarco()) {
                        if (miBarquito.getEstado().getDescripcion().equals("Destruido")) {
                            estado = "Destruido";
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                                    "Haz destruido el barco: " + miBarquito.getTipobarco().getNombre()));
                        } else if (miBarquito.getEstado().getDescripcion().equals("Tocado")) {
                            estado = "Tocado";
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                                    "Haz acertado! "));
                        }
                    }
                    verificarGanador();
                    return;
                }

            }

            estado = "Agua";
            turno1 = false;
            turno2 = true;
            manejarTurnos();
        }

    }

    public String pintarBarcosJugador1(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna) && miBarquito.getCoordenadas() != null) {
                switch (miBarquito.getTipobarco().getNombre()) {
                    case "Buque":
                        return "width: 80px; heigth: 80px; background-color: green;";

                    case "Poortaviones":
                        return "width: 80px; heigth: 80px; background-color: magenta;";

                    case "Acorazado":
                        return "width: 80px; heigth: 80px; background-color: red;";

                    case "Fragata":
                        return "width: 80px; heigth: 80px; background-color: yellow;";

                    case "Navio":
                        return "width: 80px; heigth: 80px; background-color: Brown;";

                    default:
                        return "width: 80px; heigth: 80px; background-color: PURPLE;";

                }

            }
        }
        return "width: 80px; heigth: 80px;";
    }

    public String obtenerEsiloDeCoordenada1(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenadaJuego(fila, columna) && miBarquito.getCoordenadas() != null) {
                return "width: 80px; heigth: 80px; background-color: FUCHSIA;";

            }

        }
        return "width: 80px; heigth: 80px;";
    }

    public String pintarBarcosJugador2(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna) && miBarquito.getCoordenadas() != null) {
                switch (miBarquito.getTipobarco().getNombre()) {
                    case "Buque":
                        return "width: 80px; heigth: 80px; background-color: red;";

                    case "Poortaviones":
                        return "width: 80px; heigth: 80px; background-color: orange;";

                    case "Acorazado":
                        return "width: 80px; heigth: 80px; background-color: GRAY;";

                    case "Fragata":
                        return "width: 80px; heigth: 80px; background-color: yellow;";

                    case "Navio":
                        return "width: 80px; heigth: 80px; background-color: AQUA;";

                    default:
                        return "width: 80px; heigth: 80px; background-color: PURPLE;";

                }

            }
        }
        return "width: 80px; heigth: 80px;";
    }

    public String obtenerEsiloDeCoordenada2(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenadaJuego(fila, columna) && miBarquito.getCoordenadas() != null) {
                return "width: 80px; heigth: 80px; background-color: FUCHSIA;";

            }

        }
        return "width: 80px; heigth: 80px;";
    }

    public boolean validarCoordenadaOcupadaJugador1(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador1.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna)) {
                return true;
            }

        }

        return false;
    }

    public boolean validarCoordenadaOcupadaJugador2(int fila, int columna) {
        for (BarcoPosicionado miBarquito : arbolNBatallaJugador2.listarBarcosPosicionados()) {
            if (miBarquito.validarCoordenada(fila, columna)) {
                return true;
            }

        }

        return false;
    }

    public void validarTableroLLenoJugador1() {
        for (BarcoPosicionado bar : arbolNBatallaJugador1.listarBarcos()) {
            if (bar.getCoordenadas().isEmpty()) {
                JsfUtil.addErrorMessage("Ingrese Las cordenadas Del Barco" + bar);
            }
        }
        if (arbolNBatallaJugador1.listarBarcos().isEmpty()) {
            JsfUtil.addSuccessMessage("Los Barcos Se  Posicionaron Con Exito");
            deshabilitarAsignarJugador1 = false;
        }
    }

    public void validarTableroLLenoJugador2() {
        for (BarcoPosicionado bar : arbolNBatallaJugador2.listarBarcos()) {
            if (bar.getCoordenadas().isEmpty()) {
                JsfUtil.addErrorMessage("Ingrese Las cordenadas Del Barco" + bar);
            }
        }
        if (arbolNBatallaJugador2.listarBarcos().isEmpty()) {
            JsfUtil.addSuccessMessage("Los Barcos Se  Posicionaron Con Exito");
            deshabilitarAsignarJugador2 = false;
        }
    }

    public void habilitarJuego1() {
        listo1 = true;

    }

    public void habilitarJuego2() {
        listo2 = true;

    }

    public boolean verificarListo() {
        if (listo1 && listo2) {
            return false;
        }
        return true;
    }

    

    public void MostrarGanador1() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Fin del Juego------  !!-- Ganador de Batalla Naval------- el jugador:", ""
                + arbolNBatallaJugador1.getRaiz());

        PrimeFaces.current().dialog().showMessageDynamic(message);
        turno1 = true;
        turno2 = true;
        reiniciar = false;
    }

    public void MostrarGanador2() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Fin del Juego------  !!-- Ganador de Batalla Naval------- el jugador:", "" + arbolNBatallaJugador2.getRaiz());

        PrimeFaces.current().dialog().showMessageDynamic(message);
        turno1 = true;
        turno2 = true;
        reiniciar = false;
    }

    public void verificarGanador() {

        if (arbolNBatallaJugador1.listarBarcosDestruidos().size() == (arbolNBatallaJugador1.getCantidadNodos() - 1)) {
            MostrarGanador2();

        } else if (arbolNBatallaJugador2.listarBarcosDestruidos().size() == (arbolNBatallaJugador2.getCantidadNodos() - 1)) {
            MostrarGanador1();
        }
    }

    public void manejarTurnos() {
        if (!turno1) {
            FacesMessage mensaje1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Termino El Turno", "Turno del jugador 1");
            PrimeFaces.current().dialog().showMessageDynamic(mensaje1);
        } else if (!turno2) {
            FacesMessage mensaje2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Termino El Turno", "Turno del jugador 2");
            PrimeFaces.current().dialog().showMessageDynamic(mensaje2);
        }

    }

    public String salirJuego() {
        if (!reiniciar) {
            disparosJugador1.clear();
            disparosJugador2.clear();

            contAbb.setDeshabilitarTableros(false);
            return "Salir1";
        }

        return "";
    }

    public void reiniciarJuego() {
        arbolNBatallaJugador1 = new ArbolBatallaBarcoNario();
        arbolNBatallaJugador2 = new ArbolBatallaBarcoNario();
        generarBarcosJuego();
    }

}
