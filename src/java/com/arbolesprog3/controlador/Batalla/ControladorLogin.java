/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador.Batalla;

import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author tiven
 */
@Named(value = "controladorLogin")
@ApplicationScoped
public class ControladorLogin {

    private String usuarioAdmin;
    private String contrasenaAdmin;
    private String usuarioJug;
    private String contrasenaJug;
    private String rol;
    ControladorBatallaNavalJuego controlb = (ControladorBatallaNavalJuego) FacesUtils.getManagedBean("controladorBatallaNavalJuego");

    /**
     * Creates a new instance of Login
     */
    public ControladorLogin() {
    }

    public String getUsuarioAdmin() {
        return usuarioAdmin;
    }

    public void setUsuarioAdmin(String usuarioAdmin) {
        this.usuarioAdmin = usuarioAdmin;
    }

    public String getContrasenaAdmin() {
        return contrasenaAdmin;
    }

    public void setContrasenaAdmin(String contrasenaAdmin) {
        this.contrasenaAdmin = contrasenaAdmin;
    }

    public String getUsuarioJug() {
        return usuarioJug;
    }

    public void setUsuarioJug(String usuarioJug) {
        this.usuarioJug = usuarioJug;
    }

    public String getContrasenaJug() {
        return contrasenaJug;
    }

    public void setContrasenaJug(String contrasenaJug) {
        this.contrasenaJug = contrasenaJug;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String ingresarMenuAdministrador() {
        if (usuarioAdmin.compareTo(controlb.getAdministrador().getCorreo()) == 0 && contrasenaAdmin.compareTo(controlb.getAdministrador().getPassword()) == 0) {
            return "IrAdmin";
        } else if (usuarioAdmin.compareTo(controlb.getAdministrador().getCorreo()) != 0) {
            JsfUtil.addErrorMessage("Usuario Incorrecto");
            return null;
        } else if (contrasenaAdmin.compareTo(controlb.getAdministrador().getPassword()) != 0) {
            JsfUtil.addErrorMessage("Contraseña Incorrecta");
            return null;
        }
        JsfUtil.addErrorMessage("Datos Incorrectos");
        return null;
    }

    
    
    
    
    public String ingresarMenuJugador() {
        if (usuarioJug.compareTo(controlb.getJugador1().getCorreo()) == 0 && contrasenaJug.compareTo(controlb.getJugador1().getPassword()) == 0
                && controlb.getJugador1().getTipoRol().getNombre().compareTo(rol) == 0) {
            return "IrJugador1";
        } else if (usuarioJug.compareTo(controlb.getJugador2().getCorreo()) == 0 && contrasenaJug.compareTo(controlb.getJugador2().getPassword()) == 0
                && controlb.getJugador2().getTipoRol().getNombre().compareTo(rol) == 0) {
            return "IrJugador2";

        } else if (usuarioJug.compareTo(controlb.getJugador1().getCorreo()) != 0 || usuarioJug.compareTo(controlb.getJugador2().getCorreo()) != 0) {
            JsfUtil.addErrorMessage("Usuario Incorrecto");
            return null;
        } else if (contrasenaJug.compareTo(controlb.getJugador2().getPassword()) != 0
                || contrasenaJug.compareTo(controlb.getJugador2().getPassword()) != 0) {
            JsfUtil.addErrorMessage("Contraseña Incorrecta");
            return null;
        }
        JsfUtil.addErrorMessage("Datos Incorrectos");
        return null;
    }

}
