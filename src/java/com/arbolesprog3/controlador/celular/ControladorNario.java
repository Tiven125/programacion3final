/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador.celular;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.abb.Celular;
import com.arbolesprog3.modelo.abb.Marca;
import com.arbolesprog3.modelo.abb.NodoABB;
import com.arbolesprog3.modelo.abb.Operador;
import com.arbolesprog3.modelo.arbol.nario.ArbolEneario;
import com.arbolesprog3.modelo.arbol.nario.NodoEneario;

import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author tiven
 */
@Named(value = "ControladorNario")
@SessionScoped
public class ControladorNario implements Serializable {

    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular = new Celular();
    private NodoEneario nodon;
    private ArbolEneario arbolN = new ArbolEneario();
    private ControladorABB contAbb = (ControladorABB) FacesUtils.
            getManagedBean("controladorABB");

    private String imeipadre;
    private String operador;
    private String imeiBorrar;
    private String marcaBorrar;

    private boolean verRegistrar = false;
    private boolean habilitarTabla = false;

    /**
     * Creates a new instance of ControladorArbolN
     */
    public ControladorNario() {
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public ControladorABB getContAbb() {
        return contAbb;
    }

    public void setContAbb(ControladorABB contAbb) {
        this.contAbb = contAbb;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public NodoEneario getNodon() {
        return nodon;
    }

    public void setNodon(NodoEneario nodon) {
        this.nodon = nodon;
    }

    public ArbolEneario getArbolN() {
        return arbolN;
    }

    public void setArbolN(ArbolEneario arbolN) {
        this.arbolN = arbolN;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public boolean isHabilitarTabla() {
        return habilitarTabla;
    }

    public void setHabilitarTabla(boolean habilitarTabla) {
        this.habilitarTabla = habilitarTabla;
    }

    public String getImeiBorrar() {
        return imeiBorrar;
    }

    public String getImeipadre() {
        return imeipadre;
    }

    public void setImeipadre(String imeipadre) {
        this.imeipadre = imeipadre;
    }

    public void setImeiBorrar(String imeiBorrar) {
        this.imeiBorrar = imeiBorrar;
    }

    public String getMarcaBorrar() {
        return marcaBorrar;
    }

    public void setMarcaBorrar(String marcaBorrar) {
        this.marcaBorrar = marcaBorrar;
    }

    @PostConstruct
    public void iniciar() {
        llenarMarcas();
        llenarOperadores();
        try {
            cargarArbol();
        } catch (CelularExcepcion ex) {
            Logger.getLogger(ControladorNario.class.getName()).log(Level.SEVERE, null, ex);
        }

        pintarArbol();
    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));

    }

    public void cargarArbol() throws CelularExcepcion {

        //Algoritmo
        arbolN.adicionarNodo(new Celular(0, "Padre", null, null, null, null, 0, (byte) 0), null);

        List<Celular> padres = new ArrayList<Celular>();
        padres.add(arbolN.getRaiz().getDato());
        adicionarPreOrden(contAbb.getArbol().getRaiz(), padres, 0);

    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void guardarCelular() {
        try {
            arbolN.adicionarNodo(celular, imeipadre);
            celular = new Celular();
            verRegistrar = false;
            pintarArbol();
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void cancelarGuardado() {
        verRegistrar = false;

    }

    public List<Celular> listarCelularesxOperador() {
        List<Celular> listaCelulares = new ArrayList<>();
        if (arbolN.getRaiz().getDato().getOperador().getNombre().compareTo(this.operador) == 0) {
            listaCelulares.add(arbolN.getRaiz().getDato());
        }
        listarCelularesxOperador(arbolN.getRaiz(), listaCelulares, this.operador);
        return listaCelulares;
    }

    private void listarCelularesxOperador(NodoEneario nodo, List<Celular> celulares, String operador) {
        for (NodoEneario agregar : nodo.getHijos()) {
            if (agregar.getDato().getOperador().getNombre().compareTo(operador) == 0) {
                celulares.add(agregar.getDato());
            }
            listarCelularesxOperador(agregar, celulares, operador);
        }

    }

    public void verHabilitarTabla() {
        habilitarTabla = true;

    }

    public void pintarArbol() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);

        pintarArbol(arbolN.getRaiz(), model, null, 30, 0);
    }

    private void pintarArbol(NodoEneario reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));
            }

            model.addElement(elementHijo);
            for (int i = 0; i < reco.getHijos().size(); i++) {
                pintarArbol(reco.getHijos().get(i), model, elementHijo, x - reco.getHijos().size() - 3, y + 5);
                x += 10;

            }

        }
    }

   
    
    

    public DiagramModel getModel() {
        return model;
    }

    private void adicionarPreOrden(NodoABB reco, List<Celular> padres, int contizq) throws CelularExcepcion {
        if (reco != null) {
            List<Celular> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidad(); i++) {
                Celular nuevoCelular = new Celular(reco.getDato().getCodigo(), reco.getDato().getImei(), reco.getDato().getNumeroLinea(), reco.getDato().getMarca(),
                        reco.getDato().getOperador(), reco.getDato().getColor(), reco.getDato().getPrecio(), reco.getDato().getCantidad());

                nuevoCelular.setCodigo(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolN.adicionarNodoxCodigo(nuevoCelular, padres.get(contPapas));
                padresNuevos.add(nuevoCelular);
                contPapas++;
            }
            adicionarPreOrden(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + contAbb.getArbol().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrden(reco.getDerecha(), padresNuevos, contizq);

        }
    }
    
    
     public void borrarDato() {
        arbolN.eliminarNodo(getImeiBorrar());
        JsfUtil.addSuccessMessage("Se ha borrado el celular con exito");
        pintarArbol();
    }
}
