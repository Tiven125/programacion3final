/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador.celular;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.abb.Celular;
import com.arbolesprog3.modelo.abb.Marca;
import com.arbolesprog3.modelo.abb.Operador;
import com.arbolesprog3.modelo.arbol.avl.ArbolAVL;
import com.arbolesprog3.modelo.arbol.avl.NodoAVL;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;


/**
 *
 * @author tiven
 */
@Named(value = "controladorAVL")
@SessionScoped
public class ControladorAVL implements Serializable {

    private boolean verRegistrar = false;
    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular = new Celular();
    private ArbolAVL arbol = new ArbolAVL();
    private String encontrarImei = "";
    private String PadreImei;
    private String borrarImei;

    /**
     * Creates a new instance of ControladorAVL
     */
    public ControladorAVL() {

    }

    //Getters and Setters
    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public String getPadreImei() {
        return PadreImei;
    }

    public void setPadreImei(String PadreImei) {
        this.PadreImei = PadreImei;
    }

    public String getBorrarImei() {
        return borrarImei;
    }

    public void setBorrarImei(String borrarImei) {
        this.borrarImei = borrarImei;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public ArbolAVL getArbol() {
        return arbol;
    }

    public void setArbol(ArbolAVL arbol) {
        this.arbol = arbol;
    }

    public String getEncontrarImei() {
        return encontrarImei;
    }

    public void setEncontrarImei(String encontrarImei) {
        this.encontrarImei = encontrarImei;
    }

    // MEtodos
    //metodos del arbolAVL
    @PostConstruct

    private void iniciar() {
        llenarMarcas();
        llenarOperadores();
        pintarArbol();

    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 34, 1);

    }

    private void pintarArbol(NodoAVL reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato().getImei());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 8, y + 8);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 8, y + 8);
        }
    }

    public void balancear() throws CelularExcepcion {
        arbol.balancear(arbol.getRaiz());
        pintarArbol();

    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));

    }

    public void adicionarNodo() {
        try {
            arbol.adicionarNodo(celular, arbol.getRaiz());
            JsfUtil.addSuccessMessage("El dato ha sido adicionado");
            celular = new Celular();
            verRegistrar = false;
            pintarArbol();

        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    //Nuevos 
    public String darHojas() {
        ArrayList it = this.arbol.getHojas();
        return (recorrido(it, "Hojas del Arbol"));
    }

    private String recorrido(ArrayList it, String imei) {
        int i = 0;
        String r = imei + "\n";
        while (i < it.size()) {
            r += "\t" + it.get(i).toString() + "\n";
            i++;
        }
        return (r);
    }

    public String ramaMayor() {
        ArrayList it = this.arbol.ObtenerRamamayor();
        return (recorrido(it, "Rama(s) con mas valores"));
    }

    public void borrarMenor() {
        arbol.borrarMenor();
        JsfUtil.addSuccessMessage("Celular elimado Con  Exito");
        pintarArbol();
    }

    public void borrarMayor() {
        arbol.borrarMayor();
        JsfUtil.addSuccessMessage("Celular elimado Con  Exito");
        pintarArbol();
    }

    public void borrarCelular() {
        arbol.borrarDato(getBorrarImei());
        JsfUtil.addSuccessMessage("Celular elimado Con  Exito");
        pintarArbol();
    }

    public String esta(String Bucar) {
        boolean siEsta = this.arbol.buscar(getEncontrarImei());
        String r = "El dato:" + "\n";
        r += siEsta ? "Si se encuentra en el arbol" : "No se encuentra en el arbol";

        return (r);

    }

    public void podarArbol() {
        this.arbol.podar();
        JsfUtil.addSuccessMessage("Arbol Podado");
        pintarArbol();
    }

    public void darPadre() {
        if (this.arbol.getRaiz().getDato().getImei().equals(PadreImei)) {
            JsfUtil.addErrorMessage("La raiz no tiene padres");
        }
        if (arbol.buscar(PadreImei) == false) {
            JsfUtil.addErrorMessage("El dato ingresado no existe");
            return;
        }
        String padre = this.arbol.buscarPadre(PadreImei);
        JsfUtil.addSuccessMessage("El padre de " + PadreImei + " es: " + padre);
    }
}
